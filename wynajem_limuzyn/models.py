import datetime
from django.db import models
from django.utils import timezone
from django.utils.timezone import now
class Garaz(models.Model):
    miasto = models.CharField(max_length=200)
    kod_pocztowy = models.CharField(max_length=6)
    ulica = models.CharField(max_length=200)
    numer = models.CharField(max_length=10)
    ilosc_samochodow = models.IntegerField(default=0)
    ilosc_dostepnych_samochodow = models.IntegerField(default=0)
    def __str__(self):
        return self.miasto + ", ul. " + self.ulica + " " + self.numer

    class Meta:
        verbose_name = "garaz"
        verbose_name_plural = "garaze"

class Samochod(models.Model):
    garaz = models.ForeignKey(Garaz, on_delete=models.CASCADE)
    marka = models.CharField(max_length=200)
    model = models.CharField(max_length=200)
    liczba_miejsc = models.IntegerField(default=5)
    data_produkcji = models.DateField(default=timezone.now())
    cena = models.DecimalField(max_digits=6, decimal_places=2)
    zdjecie = models.ImageField(upload_to = 'samochody')
    wypozyczony = models.BooleanField(default=False)
    def is_eco(self):
        return self.data_produkcji > datetime.date(2006, 1, 1)
    is_eco.boolean = True
    is_eco.short_description = 'Euro 4'
    def __str__(self):
        return self.marka + " " + self.model

    class Meta:
        verbose_name = "samochod"
        verbose_name_plural = "samochody"

class Wypozyczalnia(models.Model):
    samochod = models.ForeignKey(Samochod, on_delete=models.CASCADE)
    imie = models.CharField(max_length=200)
    nazwisko = models.CharField(max_length=200)
    czas_wypozyczenia_w_h = models.IntegerField(default=1)
    data_wypozyczenia = models.DateTimeField(default=now)
    data_oddania = models.DateTimeField(default=None, blank=True, null=True)
    def __str__(self):
        return self.imie + " " + self.nazwisko
    class Meta:
        verbose_name = "wypozyczalnia"
        verbose_name_plural = "wypozyczalnie"

