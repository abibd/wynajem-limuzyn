from django import forms

from .models import Wypozyczalnia

class RentForm(forms.ModelForm):

    class Meta:
        model = Wypozyczalnia
        fields = ('imie','nazwisko','czas_wypozyczenia_w_h')
        widgets = {
            'imie': forms.TextInput(attrs={'class': 'form-control'}),
            'nazwisko': forms.TextInput(attrs={'class': 'form-control'}),
            'czas_wypozyczenia_w_h': forms.TextInput(attrs={'class': 'form-control'}),
        }
