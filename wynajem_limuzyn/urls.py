from django.urls import path
from . import views


app_name = 'wynajem_limuzyn'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('search_miasto/', views.search_miasto, name='search_miasto'),
    path('<int:garaz_id>/search_samochod/', views.search_samochod, name='search_samochod'),
    path('<int:samochod_id>/wypozycz/', views.rent_samochod, name='wypozycz'),
    path('<int:samochod_id>/oddaj/', views.give_back_car, name='oddaj')
]