from django.contrib import admin
from .models import Garaz, Samochod,Wypozyczalnia


class SamochodInline(admin.TabularInline):
    model = Samochod
    extra = 0

class GarazAdmin(admin.ModelAdmin):
    inlines = [SamochodInline]
    list_display = ('__str__', 'ilosc_samochodow', 'ilosc_dostepnych_samochodow')
    list_filter = ['ilosc_samochodow', 'ilosc_dostepnych_samochodow']
    search_fields = ['miasto', 'kod_pocztowy', 'ulica', 'numer']

class SamochodAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'liczba_miejsc', 'data_produkcji', 'cena', 'is_eco', 'zdjecie')
    list_filter = ['cena', 'liczba_miejsc', 'data_produkcji']
    search_fields = ['marka', 'model']
class WypozyczalniaAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'samochod', 'imie', 'nazwisko', 'czas_wypozyczenia_w_h', 'data_wypozyczenia','data_oddania' )
    list_filter = ['samochod', 'nazwisko', 'data_wypozyczenia','data_oddania']
    search_fields = ['imie', 'nazwisko']


admin.site.register(Garaz, GarazAdmin)
admin.site.register(Samochod, SamochodAdmin)
admin.site.register(Wypozyczalnia,WypozyczalniaAdmin)