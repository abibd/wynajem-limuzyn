# Generated by Django 2.2.7 on 2019-12-15 15:16

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wynajem_limuzyn', '0006_auto_20191215_1611'),
    ]

    operations = [
        migrations.AlterField(
            model_name='samochod',
            name='rok_produkcji',
            field=models.DateField(default=datetime.date(2019, 12, 15)),
        ),
    ]
