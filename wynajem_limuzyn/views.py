from django.shortcuts import render, redirect
from django.views import generic
from .models import Garaz, Samochod, Wypozyczalnia
from django.shortcuts import render, get_object_or_404
from .forms import RentForm
from django.utils.timezone import now

class IndexView(generic.ListView):
    template_name = 'wynajem_limuzyn/index.html'
    context_object_name = 'garaze'

    def get_queryset(self):
        """
        Zwraca liste dostepnych garazy z limuzynami
        """
        return Garaz.objects.all()

class DetailView(generic.DetailView):
    model = Garaz
    template_name = 'wynajem_limuzyn/detail.html'

def search_miasto(request):
    try:
        if request.GET['miasto'] and request.GET['kod_pocztowy']:
            searched_miasto = Garaz.objects.filter(
                miasto=request.GET['miasto'],
                kod_pocztowy=request.GET['kod_pocztowy']
            )
        elif request.GET['miasto']:
            searched_miasto = Garaz.objects.filter(
                miasto=request.GET['miasto'],
            )
        elif request.GET['kod_pocztowy']:
            searched_miasto = Garaz.objects.filter(
                kod_pocztowy=request.GET['kod_pocztowy'],
            )
        else:
            searched_miasto = Garaz.objects.all
    except (KeyError, Garaz.DoesNotExist):
        return render(request, 'wynajem_limuzyn/index.html', {
            'garaze': None,
            'error_message': "Nie znaleziono garaży w wyszukiwanym mieście.",
        })
    else:
        return render(request, 'wynajem_limuzyn/index.html', {'garaze': searched_miasto})

def search_samochod(request, garaz_id):
    garaz = get_object_or_404(Garaz, pk=garaz_id)
    try:
        searched_samochod = Samochod.objects.filter(
            garaz=garaz_id,
            model=request.GET['model']
        )
    except (KeyError, Samochod.DoesNotExist):
        return render(request, 'wynajem_limuzyn/detail.html', {
            'garaz': garaz,
            'error_message': "Nie znaleziono wyszukiwanego samochodu",
        })
    else:
        garaz.samochod_set.set(searched_samochod)
        return render(request, 'wynajem_limuzyn/detail.html', {'garaz': garaz})
def rent_samochod(request,samochod_id):
    samochod = get_object_or_404(Samochod, pk=samochod_id)
    if request.method == "POST":
        form = RentForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.samochod = samochod
            post.save()
            samochod.wypozyczony = True
            samochod.save()
            garaz = get_object_or_404(Garaz, pk=samochod.garaz.pk)
            garaz.ilosc_dostepnych_samochodow = garaz.ilosc_dostepnych_samochodow - 1
            garaz.save()
            return redirect('wynajem_limuzyn:index')
    else:
        form = RentForm()

    return render(request, 'wynajem_limuzyn/rent_form.html', {'samochod': samochod,'form':form})
def give_back_car(request,samochod_id):

    if request.method == "POST":
        samochod = get_object_or_404(Samochod, pk=samochod_id)
        wypozyczenie = get_object_or_404(Wypozyczalnia, samochod_id=samochod_id,data_oddania=None)
        wypozyczenie.data_oddania = now()
        wypozyczenie.save()
        samochod.wypozyczony = False
        samochod.save()
        garaz = get_object_or_404(Garaz, pk=samochod.garaz.pk)
        garaz.ilosc_dostepnych_samochodow = garaz.ilosc_dostepnych_samochodow+1
        garaz.save()
        return redirect('wynajem_limuzyn:index')



